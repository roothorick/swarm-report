#!/usr/bin/python3

import sys, os, csv, traceback
from torf import Torrent
from urllib.parse import urlparse

from scraper import scrape

assert __name__ == "__main__"
assert len(sys.argv) >= 2

def process_torrent(filename):
    
    print() # Newline
    print( "Processing "+filename )
    
    torobj = Torrent.read(filename)
    torobj.validate() # Throws if not valid
    
    assert torobj.trackers != None
    assert type(torobj.trackers[0][0]) is str
    url = urlparse(torobj.trackers[0][0])
    
    assert url.scheme == "udp"
    assert url.hostname != None
    assert url.port != None
    
    seeders, leechers, completed = scrape(torobj)
    
    row = {}
    row["name"] = torobj.name
    row["infohash"] = torobj.infohash
    row["seeders"] = seeders
    row["leechers"] = leechers
    row["completed"] = completed
    return row

csv_fd = open("report.csv", "w")
csv_writer = csv.DictWriter(csv_fd, ["name", "infohash", "seeders", "leechers", "completed"])
csv_writer.writeheader()

for dir in sys.argv[1:]:
    for filename in os.listdir(dir):
        if filename.endswith(".torrent"):
            try:
                csv_writer.writerow( process_torrent( dir + "/" + filename) )
            except Exception:
                traceback.print_exc()

csv_fd.close()
