from random import randrange
from time import time
from socket import socket, AF_INET, SOCK_DGRAM, timeout
from urllib.parse import urlparse
from struct import pack, unpack
from binascii import unhexlify

class TorrentTrackerError(Exception):
    pass

# Taken from BEP 15
UDPTRACKER_MAGIC = 0x41727101980
__CONNECTION_TOKEN_LIFETIME = 60
ACTION_CONNECT = 0
ACTION_SCRAPE = 2
ACTION_ERROR = 3

# (host, port), (timestamp, token)
__connection_tokens = {}

__our_socket = socket(AF_INET, SOCK_DGRAM)

__retry_count = 0

# Get a valid connection token
def __connection_token(host, port):
    global __connection_tokens
    if (host, port) in __connection_tokens:
        if __connection_tokens[ (host,port) ][0] + __CONNECTION_TOKEN_LIFETIME > time():
            return __connection_tokens[ (host,port) ][1]
        else:
            del __connection_tokens[ (host,port) ]
    
    return __new_connection_token(host, port)

# Get a NEW connection token
def __new_connection_token(host, port, transaction_token=-1):
    global __connection_tokens, __our_socket, __retry_count
    assert (host,port) not in __connection_tokens
    
    if(transaction_token == -1):
        transaction_token = randrange(0, 2**32)
    
    __our_socket.connect( (host,port) )
    
    request = pack("!QLL", UDPTRACKER_MAGIC, ACTION_CONNECT, transaction_token)
    print("Sent    : " + request.hex() )
    __our_socket.send(request)
    
    # XXX: A lot of this is duplicated in scrape() but not in away that is easy to merge
    __our_socket.settimeout(15*2**__retry_count) # See BEP 15
    try:
        response = __our_socket.recv(1500) # Don't assume packet size
        print("Received: " + response.hex() )
        print() # newline
        
        action, response_token = unpack("!LL", response[0:8])
        assert response_token == transaction_token
        if action == ACTION_ERROR:
            raise TorrentTrackerError( response[8:].decode() )
        
        assert action == ACTION_CONNECT
        
        new_connection_token = unpack("!Q", response[8:20] )[0]
        
        
        __connection_tokens[ (host,port) ] = (time(),new_connection_token)
        return new_connection_token
        
    except timeout:
        __retry_count +=1
        if __retry_count > 8:
            raise
        
        return __new_connection_token(host, port, transaction_token)

def __scrape(torobj, transaction_token=-1):
    global __our_socket, __retry_count
    torobj.validate() # Will throw if invalid
    tracker_url = urlparse( torobj.trackers[0][0] )
    if tracker_url.scheme != "udp":
        raise NotImplementedError("Only UDP trackers are supported")
    
    if(transaction_token == -1):
        transaction_token = randrange(0, 2**32)
    
    __our_socket.connect( (tracker_url.hostname,tracker_url.port) )
    
    request = pack("!QLL", __connection_token(tracker_url.hostname,tracker_url.port), ACTION_SCRAPE, transaction_token)
    request += unhexlify(torobj.infohash)
    print("Sent    : " + request.hex() )
    __our_socket.send(request)
    
    __our_socket.settimeout(15*2**__retry_count) # See BEP 15
    try:
        response = __our_socket.recv(1500) # Don't assume packet size
        print("Received: " + response.hex() )
        print() # Newline
        
        action, response_token = unpack("!LL", response[0:8])
        assert response_token == transaction_token
        if action == ACTION_ERROR:
            raise TorrentTrackerError( response[8:].decode() )
        
        assert action == ACTION_SCRAPE
        
        seeders, completed, leechers = unpack("!LLL", response[8:20])
        return seeders, leechers, completed
    
    except timeout:
        __retry_count +=1
        if __retry_count > 8:
            raise
        
        return __scrape(torobj, transaction_token)

def scrape(torobj):
    global __retry_count
    __retry_count = 0
    return __scrape(torobj,-1)
